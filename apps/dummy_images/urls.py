from django.conf.urls import url

from apps.dummy_images.views import ThumbnailView

urlpatterns = [
    url('(?P<width>\d+)x(?P<height>\d+)/$', ThumbnailView.as_view())
]
