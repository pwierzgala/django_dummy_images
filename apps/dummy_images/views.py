# -*- coding: utf-8 -*-

from django.core.cache import cache
from django.http import HttpResponse, Http404
from django.views import View
from PIL import Image, ImageOps
from StringIO import StringIO

from apps.dummy_images.models import DummyImage


class ThumbnailView(View):

    def get(self, request, width, height):
        dummy_image = DummyImage.objects.order_by('?').first()
        if dummy_image is None:
            raise Http404

        width, height = int(width), int(height)
        resolution = "{}x{}".format(width, height)
        thumbnail = cache.get(resolution)
        if thumbnail is None:
            image = Image.open(dummy_image.image.path)
            image_ops = ImageOps.fit(image, (width, height), Image.ANTIALIAS)
            thumb_bufer = StringIO()
            image_ops.save(thumb_bufer, format='jpeg')
            thumbnail = thumb_bufer.getvalue()
            cache.set(resolution, thumbnail, 3600)

        return HttpResponse(thumbnail, content_type="image/jpg")
