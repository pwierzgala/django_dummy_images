from django.contrib import admin

from apps.dummy_images.models import DummyImage

admin.site.register(DummyImage)
